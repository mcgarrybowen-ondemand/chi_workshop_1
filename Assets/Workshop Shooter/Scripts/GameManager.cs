﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Nowlab.WorkShop.Demo
{
	public class GameManager: MonoBehaviour   {

		// Singleton Accessor
		private static GameManager _instance;
		public static GameManager Instance { get { return _instance; } }

		// Score
		public Text scoreText;
		public int score = 0;

		void Awake() {
			_instance = this;
			DontDestroyOnLoad (gameObject);
		}

		void Start () {
			Cursor.lockState = CursorLockMode.Confined;
			Cursor.visible = false;

			ResetGame ();
		}

		public void ResetGame()
		{
			score = 0;
			scoreText.text = score.ToString ();
		}

		public void IncrementScore(int value) {
			Debug.Log ("incrementScore: " + value);
			score += value;
			scoreText.text = score.ToString ();
			AsteroidManager.Instance.LevelUp ();
		}


	}
}

