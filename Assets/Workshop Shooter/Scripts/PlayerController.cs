﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Nowlab.WorkShop.Demo
{
    [RequireComponent(typeof(MouseLook))]
	public class PlayerController : MonoBehaviour
	{

		public Image hit;
		private Camera fpsCam;

		void Start ()
		{

			// Set a reference to the camera
			fpsCam = GetComponent<Camera> ();

			// Initialize the hit effect
			hit.GetComponent<Image> ().enabled = true;
			hit.canvasRenderer.SetAlpha (0f);

		}

		private void OnTriggerEnter (Collider other)
		{
			Asteroid target = other.GetComponent<Asteroid> ();

			if (target != null) {
				TakeDamage ();
				Destroy (target.gameObject);
			}
		}

		private void TakeDamage ()
		{
			hit.canvasRenderer.SetAlpha (1f);
			hit.CrossFadeAlpha (0f, 0.2f, true);
		}

	}
}
