﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Nowlab.WorkShop.Demo
{
	[RequireComponent (typeof(LineRenderer))]
	public class RaycastShoot : MonoBehaviour
	{
		// how much health damage is inflicted on a target
		public int gunDamage = 1;
		// delay between shots
		public float fireRate = 0.25f;
		// how far the weapon can fire
		public float weaponRange = 50f;
		// amount of physical force to apply to a hit target
		public float hitForce = 100f;
		// location of muzzle on the gun
		public Transform gunEnd;

		// the player's camera
		private Camera fpsCam;
		// duration (lifespan) of the gunshot
		private WaitForSeconds shotDuration = new WaitForSeconds (0.07f);
		// sound to play when player shoots
		private AudioSource gunAudio;
		// visual render of the laser shot
		private LineRenderer laserLine;
		// holds reference to the next time a player can shoot
		private float nextFire;

		public GameObject explosionPrefab;

		void Start ()
		{
			// initialize variables
			laserLine = GetComponent<LineRenderer> ();
			gunAudio = GetComponentInParent<AudioSource> ();
            fpsCam = Camera.main;
		}


		void Update ()
		{
			// Check to see if the player fires, or is firing, and is allowed to fire
			if ((Input.GetButtonDown ("Fire1") || Input.GetButton ("Fire1")) && Time.time > nextFire) {

				// set the delay for the next shot
				nextFire = Time.time + fireRate;

				// shoot the laser
				ShootLaser ();
			}
		}

		private void ShootLaser ()
		{
			// shoot the laser
			StartCoroutine (LaserEffect ());

			// detect a hit using a RayCast
			Vector3 rayOrigin = fpsCam.ViewportToWorldPoint (new Vector3 (0.5f, 0.5f, 0));
			RaycastHit hit;

			laserLine.SetPosition (0, gunEnd.position); 

			if (Physics.Raycast (rayOrigin, fpsCam.transform.forward, out hit, weaponRange)) {

				// laser hit a target
				// render line to the target
				laserLine.SetPosition (1, hit.point);

				Asteroid asteroid = hit.collider.GetComponent<Asteroid> ();

				if (asteroid != null) {
					// deal some damage!
                    Debug.Log("gunDamage: " +gunDamage);
					asteroid.Damage (gunDamage);
				}

				// if the target has a rigid body attached, then add some physics to the hit
				if (hit.rigidbody != null) {
					hit.rigidbody.AddForce (-hit.normal * hitForce);
				}

			} else {
				// didn't hit anything, render line to weapon range
				laserLine.SetPosition (1, rayOrigin + (fpsCam.transform.forward * weaponRange));
			}
		}

		/// <summary>
		/// play the gun audio,
		/// enable the line renderer,
		/// and disable the line renderer once the shot lifespan times out
		/// </summary>
		/// <returns></returns>
		private IEnumerator LaserEffect ()
		{
			// play the laser sound effect
			gunAudio.Play ();

			laserLine.enabled = true;
			yield return shotDuration;
			laserLine.enabled = false;
		}
	}
}