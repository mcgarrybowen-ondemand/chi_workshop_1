﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Nowlab.WorkShop.Demo
{
	[RequireComponent (typeof(Rigidbody))]
	public class Asteroid : MonoBehaviour
	{
        // hit points required to destroy 
        public int currentHealth = 1;

        // monetary value of destroying the asteroid
        public int pointValue = 1;

        // position/rotation/scale of the player
        private Transform player;

        // Physics component of the asteroids
        private Rigidbody rigidbody;

        // flag to determine if we're close to player
        private bool closeToPlayer = false;

        // flag to determine if asteroid is active (not destroyed)
        private bool isActive = true;

        // explosion effect to use when asteroid is destroyed
		public GameObject explosionPrefab;

		void Start ()
		{
			player = GameObject.FindGameObjectWithTag ("Player").transform;
			rigidbody = GetComponent<Rigidbody> ();

			// add force to move asteroid toward player
			// *normalizing* a vector takes the magnitude out of it, leaving you with just a direction
			//  (example: new Vector3(0,2,0).normalized  is the same as Vector3.up [0,1,0]
			rigidbody.velocity = (player.position - transform.position).normalized * Random.Range (0.1f, 1f);

			// choose a random point on the asteroid to add rotational force to
			rigidbody.angularVelocity = Random.onUnitSphere * Random.Range (1f, 3f);
		}

		private void FixedUpdate ()
		{
			// current velocity 
			Vector3 velocity = rigidbody.velocity;

			// distance from player
			Vector3 currDist = (player.position - transform.position);

			if (currDist.magnitude < 0.5f) {
				closeToPlayer = true;
			}

			// make the asteroid move faster so gameplay isn't so simple
			if (!closeToPlayer) {
				rigidbody.velocity = currDist.normalized * velocity.magnitude;
			}
		}

		public void Damage (int damageAmount)
		{
			currentHealth -= damageAmount;

			if (currentHealth <= 0 && isActive) {
				Explode ();
			}
		}


		private void Explode ()
		{
			Destroy (gameObject);
            Instantiate(explosionPrefab, transform.position, Quaternion.identity);
        }
	}
}
