﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Nowlab.WorkShop.Demo
{
	public class RadarRenderableObject : MonoBehaviour
	{

		public Image image;

		void Start ()
		{
			Radar.RegisterRadarObject (this.gameObject, image);
		}


		private void OnDestroy ()
		{
			Radar.RemoveRadarObject (this.gameObject);
		}
	}
}
