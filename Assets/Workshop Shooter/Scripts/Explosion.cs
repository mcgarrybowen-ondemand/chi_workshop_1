﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Nowlab.WorkShop.Demo
{
	public class Explosion : MonoBehaviour
	{

		public float destroyAfterSeconds = 4.0f;

		void Awake ()
		{
			// Destroy gameobject after delay
			Destroy (gameObject, destroyAfterSeconds);
		}
	}

}