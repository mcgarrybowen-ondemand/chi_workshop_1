﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Nowlab.WorkShop.Demo
{
	public class AsteroidManager : MonoBehaviour
	{

		// singleton accessor
		private static AsteroidManager _instance;

		public static AsteroidManager Instance { get { return _instance; } }

		public GameObject[] prefabs;

		[SerializeField]
		private float spawnDelay = 5f;
		public bool spawnAsteroids = true;


		void Awake ()
		{
			_instance = this;
		}

		IEnumerator Start ()
		{
			while (spawnAsteroids) {

				// get a random starting position
				Vector3 pos = new Vector3 (Random.Range (-10f, 10f), Random.Range (-2f, 2f), Random.Range (-10f, 10f));

				// Range with an integer is not maximally inclusive
				GameObject prefab = prefabs [Random.Range (0, prefabs.Length)];

				// instantiate the asteroid at the random starting position
				GameObject astroid = (GameObject)Instantiate (prefab, pos, Quaternion.identity);

				// make the asteroid a child of the Asteroid Manager (to keep the hierarchy clean)
				astroid.transform.parent = gameObject.transform;

				// delay before spawning another asteroid
				yield return new WaitForSeconds (spawnDelay);
			}
		}

		public void LevelUp ()
		{
			spawnDelay -= 0.1f;

			if (spawnDelay < 0f) {
				spawnDelay = 0f;
			}
		}


	}

}
