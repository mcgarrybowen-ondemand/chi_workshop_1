﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Nowlab.WorkShop.Demo
{
	public class RadarObject
	{
		public Image icon { get; set; }

		public GameObject owner { get; set; }
	}

	public class Radar : MonoBehaviour
	{

		public Transform playerPos;
		public float mapScale = 10.0f;

		public static List<RadarObject> radarObjects = new List<RadarObject> ();

		public static void RegisterRadarObject (GameObject go, Image i)
		{
			Image image = Instantiate (i);
			radarObjects.Add (new RadarObject () { owner = go, icon = image });
		}

		public static void RemoveRadarObject (GameObject go)
		{
			List<RadarObject> newList = new List<RadarObject> ();
			for (int i = 0; i < radarObjects.Count; i++) {
				if (radarObjects [i].owner == go) {
					Destroy (radarObjects [i].icon);
					continue;
				} else {
					newList.Add (radarObjects [i]);
				}
			}

			radarObjects.RemoveRange (0, radarObjects.Count);
			radarObjects.AddRange (newList);
		}

		void DrawRadarDots ()
		{
			foreach (RadarObject ro in radarObjects) {
				Vector3 radarPos = (ro.owner.transform.position - playerPos.position);
				float distToObject = Vector3.Distance (playerPos.position, ro.owner.transform.position) * mapScale;
				float dY = Mathf.Atan2 (radarPos.x, radarPos.z) * Mathf.Rad2Deg - 270 - playerPos.eulerAngles.y;
				radarPos.x = distToObject * Mathf.Cos (dY * Mathf.Deg2Rad) * -1;
				radarPos.z = distToObject * Mathf.Sin (dY * Mathf.Deg2Rad);

				ro.icon.transform.SetParent (this.transform);
				ro.icon.transform.position = new Vector3 (radarPos.x, radarPos.z, 0) + this.transform.position;
			}
		}

		private void Update ()
		{
			DrawRadarDots ();
		}
	}
}